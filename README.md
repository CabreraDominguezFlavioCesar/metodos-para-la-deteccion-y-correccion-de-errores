# FUNDAMENTOS DE TELECOMUNICACIONES - ISB

## Métodos para la detección y corrección de errores -Mapa Conceptual

```plantuml
@startmindmap

* Metodos para la deteccion\nycorreccion de errores
** Metodos para la deteccion\nycorreccion de errores
*** Paridad simple (paridad horizontal)

*** Suma de comprobación
**** Es un método sencillo pero eficiente sólo con cadenas de palabras de una longitud pequeña,\nes por esto que se suele utilizar en cabeceras de tramas importantes u otras cadenas importantes\ny en combinación con otros métodos
**** Funcionalidad
***** consiste en agrupar el mensaje a transmitir en cadenas de una longitud determinada L no muy grande, de por ejemplo 16 bits\nConsiderando a cada cadena como un número entero numerado según el sistema de numeración\nA continuación se suma el valor de todas las palabras en las que se divide el mensaje\ny se añade el resultado al mensaje a transmitir, pero cambiado de signo.

*** Distancia de Hamming basada en comprobación
**** Si queremos detectar d bit erróneos en una palabra de n bits,\npodemos añadir a cada palabra de n bits d+1 bits predeterminados al final,\nde forma que quede una palabra de n+d+1 bits con una distancia mínima de Hamming de d+1
**** De esta manera, si uno recibe una palabra de n+d+1 bits que no encaja con ninguna palabra del código\n(con una distancia de Hamming x <= d+1 la palabra no pertenece al código) detecta correctamente si es una palabra errónea\nAún más, d o menos errores nunca se convertirán en una palabra válida debido a que la distancia de Hamming entre cada palabra\nválida es de al menos d+1, y tales errores conducen solamente a las palabras inválidas que se detectan correctamente.

** Verificación VRC, LRC, CRC
*** VERIFICACIÓN DE ERRORES
**** La codificación binaria es de gran utilidad práctica en dispositivos electrónicos como ordenadores,\ndonde la información se puede codificar basándose en la presencia o no de una señal eléctrica
**** Sin embargo, esta señal eléctrica puede sufrir alteraciones (como distorsiones o ruidos),\nespecialmente cuando se transportan datos a grandes distancias\nPor este motivo, ser capaz de verificar la autenticidad de estos datos es imprescindible para ciertos propósitos\n(incluido el uso de información en entornos profesionales,\nbancarios, industriales, confidenciales o relacionados con la seguridad).
*** VERIFICACIÓN DE REDUNDANCIA LONGITUDINAL
**** Digamos que HELLO es el mensaje que transmitiremos utilizando el estándar ASCII. Estos son los datos tal como se transmitirán con los códigos de verificación de redundancia longitudinal
**** Letra	Código ASCII\n  (7 bits)	Bit de paridad(LRC)\nH	1001000	0\nE	1000101	1\nL	1001100	1\nL	1001100	1\n0	1001111	1\nVRC	1000010	0

*** VERIFICACIÓN DE REDUNDANCIA LONGITUDINAL
**** La verificación de redundancia clongitudinal (abreviado, VRC )\nes un método de control de integridad de datos de fácil implementación.\nEs el principal método de detección de errores utilizado en las telecomunicaciones
*** CONCEPTO
**** La verificación de redundancia cíclica consiste en la protección de los datos en bloques, denominados tramas.\nA cada trama se le asigna un segmento de datos denominado código de control\n(al que se denomina a veces FCS, secuencia de verificación de trama, en el caso de una secuencia de 32 bits, y que en ocasiones se identifica erróneamente como VRL).\nEl código VRL contiene datos redundantes con la trama, de manera que los errores no sólo se pueden detectar sino que además se pueden solucionar
*** VERIFICACION de REDUNDANCIA CICLICA (CRC)
**** El concepto de CRC consiste en tratar a las secuencias binarias como polinomios binarios,\ndenotando polinomios cuyos coeficientes se correspondan con la secuencia binaria\nPor ejemplo, la secuencia binaria 0110101001 se puede representar como un polinomi\ncomo se muestra a continuación
**** 0*X9 + 1*X8 + 1*X7 + 0*X6 + 1*X5 + 0*X4 + 1*X3 + 0*X2 + 0*X1 + 1*X0 \n X8 + X7 + X5 + X3 + X0 \n X8 + X7 + X5 + X3 + 1
**** De esta manera, la secuencia de bits con menos peso (aquella que se encuentra más a la derecha)\nrepresenta el grado 0 del polinomio (X0 = 1), (X0 = 1), (X0 = 1),\nel 4º bit de la derecha representa el grado 3 del polinomio (X3), y así sucesivamente\nLuego, una secuencia de n- bits forma un polinomio de grado máximo n-1 \nTodas las expresiones de polinomios se manipulan posteriormente utilizando un módulo 2.
**** En este proceso de detección de errores, un polinomio predeterminado (denominado polinomio generador y abreviado G(X)) \nes conocido tanto por el remitente como por el destinatario \nEl remitente, para comenzar el mecanismo de detección de errores, ejecuta un algoritmo en los bits de la trama, de forma que se genere un CRC,\ny luego transmite estos dos elementos al destinatario. El destinatario realiza el mismo cálculo a fin de verificar la validez del CRC.

** CONTROL DE FLUJO
*** Control de Flujo es una técnica para sincronizar el envío de paquetes entre dos máquinas,\nlas que eventualmente procesarán esta información a velocidades irregulares por lo que \nse hace necesario un control de flujo entre los datos transmitidos \nEl protocolo TCP proporciona el servicio de Control de Flujo a sus aplicaciones\npara eliminar la posibilidad de que el emisor desborde el buffer del receptor.
*** Tipos
**** ASENTAMIENTO
***** Un primer protocolo capaz de controlar la congestión muy simple es el conocido como de parada y\nespera o en términos más formales se conoce como Asentamiento. \nÚnicamente para evitar desbordar al receptor, el emisor enviaría una trama y esperaría un acuse de recibo antes de enviar la siguiente. \nEste procedimiento resulta adecuado cuando hay que enviar pocas tramas de gran tamaño. \nSin embargo, la información suele transmitirse en forma de tramas cortas debido a la posibilidad de errores, \nla capacidad de buffer limitada y la necesidad en algunos casos de compartir el medio.
**** VENTANAS DESLIZANTES
***** Un mecanismo más sofisticado y muy empleado es el de la  ventana deslizante. \nLa ventana determina cuantos mensajes pueden estar pendientes de confirmación y su tamaño se ajusta a la capacidad del buffer del receptor para almacenar tramas. \nEl tamaño  máximo de la ventana está además limitado por el tamaño del número de secuencia que se utiliza para numerar las tramas.
**** CONTROL POR HARDWARE
***** Consiste en utilizar líneas dispuestas para ese fin como las que tiene la conexión RS-232-C. \nEste método de control del flujo de transmisión utiliza líneas del puerto serie para \nparar o reanudar el flujo de datos y por tanto el cable de comunicaciones, además de las tres líneas fundamentales de la conexión serie\n emisión, recepción y masa, ha de llevar algún hilo más para transmitir las señales de control
***** En el caso más sencillo de que la comunicación sea en un solo sentido, por ejemplo con una impresora, \nbastaría con la utilización de una línea más. Esta línea la gobernaría la impresora y su misión sería \nla de un semáforo. Por ejemplo, utilizando los niveles eléctricos reales que usa la norma serie RS-232-C, \nsi esta línea está a una tensión positiva de 15 V. (0 lógico) indicaría que la impresora está\nen condiciones de recibir datos, y si por el contrario está a -15 V. \n(1 lógico) indicaría que no se le deben enviar más datos por el momento.
**** CONTROL POR SOFTWARE
***** La otra forma de control del flujo consiste en enviar a través de la línea de comunicación caracteres \nde control o información en las tramas que indican al otro dispositivo el estado del receptor. \nLa utilización de un control software de la transmisión permite una mayor versatilidad del protocolo de comunicaciones \ny por otra parte se tiene mayor independencia del medio físico utilizado. \nAsí por ejemplo, con un  protocolo exclusivamente hardware sería bastante difícil hacer una comunicación vía telefónica, \nya que las señales auxiliares de control se tendrían  que emular de alguna manera.





@endmindmap
```

## Bibliografia

https://sites.google.com/site/sistemasdemultiplexado/arquitecturas-de-las-redes-de--comunicacin-caractersticas/8--deteccin-y-correccin-de-errores

http://fundamentostelecom.blogspot.com/2012/12/24-control-de-flujo-tipos-asentimiento.html

Libros:
    TRANSMISION DE DATOS Y REDES DE COMUNICACIONES - Behrouz A. Forouzan
    MANUAL DE TELECOMUNICACIONES - Jose M. Huidobro
    COMUNICACIONES Y REDES DE COMPUTADORES - William Stallings